const hapi = require ('@hapi/hapi')
const Joi = require('@hapi/joi');
const Inert = require('@hapi/inert');
const pets = require('./pets.json')
const Bcrypt = require('bcrypt');
const Qs = require('qs')
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/dkata',{useNewUrlParser: true});

const pet= mongoose.model('pet',{
    id: Number,
    name: String,
    breed: String,
    colour: String,
    age: Number,
    next_checkup: Date,
    vaccinations: Array,
    sold:Boolean

})

pets.forEach(element => {
    Object.assign(element,{sold:false})
});

const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret' 
        name: 'John Doe',
        id: '2133d32a'
    }
};
const validate = async (request, username, password) => {
 
    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

const init = async()=>{
    const server= hapi.server({
        port: 1509,
        host: 'localhost', 
        query:{
            parser:(query) => Qs.parse(query)
        }
    })
    await server.register(require('@hapi/basic'));

    server.auth.strategy('simple', 'basic', { validate });
    await server.register(Inert)
    //get one id in file pets.json
    
    server.route({
        method: 'GET',
        path: '/allpet',
        handler: function (request, h) {

            return pet.find();
        }
    })

    server.route({
        method: 'GET',
        path: '/pets',
        handler: async function (request, h) {
            const query= request.query
            if(!query.sort && !query.limit && !query.offset && !query.filter){
                return pet.find({}, {_id:0,__v:0,sold:0},(err,res)=>{
                    if(err){
                        return h.response(err).code(500)
                    } else{
                        return h.response(res).code(200)
                    }
                })
            } else{
                let  sort = query.sort?query.sort:{'sold':'name'}
                let offset =query.offset?parseInt(query.offset):0
                let limit = query.limit?parseInt(query.limit):10
                let filter = query.filter?query.filter:{sold: false}
                console.log(filter)
                const result= await pet.find({sold:false},{_id:0,__v:0})
                .sort({[sort]:1})
                .skip(offset)
                .limit(limit)
                .lean()
                return h.response(result).code(200) 
            }
        },
    })
    server.route({
        method: 'GET',
        path: '/pets/{id}',
        handler: function (request, h) {
            let petId= request.params.id
            return pet.findOne({id: petId})
        },
    })
    
    server.route({
        method:'POST',
        path: '/pets',
        handler: async function(request, h){
            const idPet= await pet.aggregate([
                {
                    $group:{
                        _id:'max',
                        maxId:{
                            $max:'$id',
                        },
                        names:{
                            $push:'$name'
                        }
                    }
                }
            ])
            let nextId=idPet[0].maxId+1
            Object.assign(request.payload,{id:nextId,sold:false})
            await pet.insertMany([request.payload])
            return h.response(request.payload).code(201)
        }
    })

    server.route({
        method: 'PUT',
        path:'/pets/{id}',
        handler: async function(request,h){
            try {
                var result = await pet.findOneAndUpdate({id:request.params.id}, request.payload, { new: true });
                return h.response(result);
            } catch (error) {
                return h.response(error).code(202);
            }
        }
    })

    server.route({
        method:'DELETE',
        path:'/pets/{id}',
        handler: async function(request,h){
            try {
                var result = await pet.findOneAndUpdate({id:request.params.id}, { sold: true });
                return h.response(result);
            } catch (error) {
                return h.response(error).code(201);
            }
        },
    })
    process.on('unhandledRejection', (err) => {
        console.log(err)
        process.exit(1)
    })
     await server.start()
    console.log('Server running on %s', server.info.uri)
    //untuk tes yg ini dijalankan yg init() di comment
    return server
}
init()